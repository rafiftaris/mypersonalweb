localStorage.setItem("themes", JSON.stringify([
    {"id": 0, "text": "Summer Dog", "bg-img": "linear-gradient(to top right, #43C6AC, #F8FFAE)", "footer-color" : "#27A275"},
    {"id": 1, "text": "Moonlit Asteroid", "bg-img": "linear-gradient(to right, #0f2027, #203a43, #2c5364)", "footer-color" : "#698795"},
    {"id": 2, "text": "Citrus Peel", "bg-img": "linear-gradient(to right, #f37335, #fdc830)", "footer-color" : "#D54F0F"},
    {"id": 3, "text": "Cosmic Fusion", "bg-img": "linear-gradient(to right, #ff00cc, #333399)", "footer-color" : "#7C14FF"}
]));

$(document).ready(function () {
    $('.theme-select').select2({
        data: JSON.parse(localStorage.getItem("themes"))
    });


    $('.apply-button').on('click', function () {
        theme = JSON.parse(localStorage.getItem('themes'))[$('.theme-select').val()];
        $('body').css({"background-image": theme['bg-img']});
        $('.footer').css({"background-color": theme['footer-color']});
        localStorage.setItem('selectedTheme', JSON.stringify(theme));
    });
});

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight != "0%"){
      panel.style.maxHeight = "0%";
    } else {
      panel.style.maxHeight = "100%";
    } 
  });
}   