from django.urls import path
from .views import *

app_name = 'mysite'
urlpatterns = [
	path('',index, name='index'),
	path('main/',mainpage, name='mainpage'),
	path('message_me/',message, name='message'),

    path('registration/',registration, name='registration'),
    path('registration/verification',email_verification, name="email_verification"),
    path('registration/add_subscriber/',add_subscriber, name='add_subscriber'),
    path('registration/unsubscribe/<email>/<password>/',unsubscribe, name='unsubscribe'),


    path('schedule_form/',schedule_form,name='schedule_form'),
    path('schedule_reset/',schedule_reset,name='schedule_reset'),
    path('schedule/',schedule_view, name='schedule_view'),
]