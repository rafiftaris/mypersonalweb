from django.shortcuts import render,redirect
from .models import Schedule, Registration
from .forms import Schedule_Form, Registration_Form
from django.core.validators import validate_email
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

# Create your views here.
def index(request):
    return render(request,'Story03WelcomePage.html',{})

def mainpage(request):
	return render(request,'Story03.html',{})	

def thanks_regist(request):
	return render(request,'thanks.html')

def message(request):
	return render(request,'Story03Challenge.html')

def registration(request):
	form = Registration_Form(request.POST or None)
	response = {}
	response['form'] = form
	return render(request,'Registration.html',response)

@csrf_exempt
def add_subscriber(request):
	print('post')
	if (request.method == 'POST'):
		enctype = "multipart/form-data"
		name = request.POST.get("name")
		email = request.POST.get("email")
		password = request.POST.get("password")
		registration = Registration(name=name,email=email,password=password)
		registration.save()

		data = model_to_dict(registration)

		return HttpResponse(data) 
	else:
		registeredAccounts = Registration.objects.all().values()
		account_list = list(registeredAccounts)
		return JsonResponse(account_list, safe = False)

def model_to_dict(obj):
	data =	serializers.serialize('json',[obj,])
	struct = json.loads(data)
	data = json.dumps(struct[0]['fields'])
	return data


@csrf_exempt
def email_verification(request):
	email_valid = False
	if(request.method == 'POST'):
		email = request.POST.get('email')
		
		if(len(email)>=1): 
			email_valid = True

		data = {'is_taken' : Registration.objects.filter(email = email).exists(), 'is_valid' : email_valid}
		return JsonResponse(data)

@csrf_exempt
def unsubscribe(request,email,password):
	if request.method == 'POST':
		email = request.POST['email']
		print(email)
		sub = Registration.objects.get(email=email)
		print('sub')

		if sub.password == password:
			print('berhasil')
			sub.delete()
			return JsonResponse({})

		else:
			print('gagal')
			return JsonResponse({})
	else:
		return HttpResponse({})


def schedule_form(request):
	form = Schedule_Form(request.POST or None)
	response = {}	
	if (request.method == 'POST'):
		if(form.is_valid()):
			response['event'] = request.POST.get("event")
			response['date'] = request.POST.get("date")
			response['time'] = request.POST.get("time")
			response['venue'] = request.POST.get("venue")
			response['category'] = request.POST.get("category")
			schedule = Schedule(date=response['date'],time=response['time'],event=response['event'],venue=response['venue'],category=response['category'])
			schedule.save()
	response['form'] = form
	return render(request,"ScheduleForm.html", response)		
			


def schedule_view(request):
	schedule = Schedule.objects.all()
	response = {
		"jadwal" : schedule
	}
	return render(request, 'ScheduleView.html', response)

def schedule_reset(request):
	Schedule.objects.all().delete()
	return render(request, 'ScheduleView.html', {})