from django.test import TestCase,Client
from django.urls import resolve
from .views import *


# Create your tests here.
class MysiteTest(TestCase):
	def test_mysite_url_is_exist(self):
		response = Client().get('/welcome/main/')
		self.assertEqual(response.status_code,200)

	def test_mysite_using_landing_page_function(self):
		found = resolve('/welcome/main/')
		self.assertEqual(found.func, mainpage)

	def test_mysite_using_landing_page_template(self):
		response = Client().get('/welcome/main/')
		self.assertTemplateUsed(response,'Story03.html')

class lab10test(TestCase):
	def test_url_profile_is_exist(self):
		response = Client().get('/welcome/registration/')
		self.assertEqual(response.status_code,200)

	def test_url_regist_is_exist(self):
		response = Client().get('/welcome/registration/')
		self.assertEqual(response.status_code,200)

	def test_registrasi_using_regis_template(self):
		response = Client().get('/welcome/registration/')
		self.assertTemplateUsed(response,'Registration.html')

	def test_using_regist_func(self):
		found = resolve('/welcome/registration/')
		self.assertEqual(found.func,registration)

	def test_model_can_add_subscriber(self):
		add_subscriber = Registration.objects.create(name = "john",email = "john@yahoo.com", password = "johnjohn")
		count_obj = Registration.objects.all().count()
		self.assertEqual(count_obj,1)

	def test_add_subscriber(self):
		response = Client().post('/welcome/registration/',{'name':"name",'email':"email",'password':"password"})
		self.assertEqual(response.status_code,200)
