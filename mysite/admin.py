from django.contrib import admin
from .models import Schedule,Registration
# Register your models here.
admin.site.register(Schedule)
admin.site.register(Registration)
