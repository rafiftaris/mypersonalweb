from django import forms

class Schedule_Form(forms.Form):
	date = forms.DateField(label='Date', required=True, widget=forms.DateInput(attrs={'type' : 'date'}))
	time = forms.TimeField(label='Time', required=True, widget=forms.TimeInput(attrs={'type' : 'time'}))
	event = forms.CharField(label='Event', required=True, max_length=25)
	venue = forms.CharField(label='Venue', required=True, max_length=50)
	category = forms.CharField(label='Category', required=True, max_length=10)

class Registration_Form(forms.Form):
	name = forms.CharField(
		label='Name',
		# required=True,
		max_length=50, 
		widget=forms.TextInput(
			attrs={
				'class' : 'form-control',
				'width' : ' 30%', 
				'id' : 'name-field',
				'placeholder' : 'John Doe'
			}
		)
	)
	email = forms.EmailField(
		label='Email',
		# required=True, 
		widget=forms.TextInput(
			attrs={
				'class' : 'form-control', 
				'width' : ' 30%', 
				'id' : 'email-field',
				'placeholder' : 'example@email.com'
			}
		)
	)
	password = forms.CharField(label='Password',
		# required=True,
		widget=forms.PasswordInput(
			attrs={
				'class' : 'form-control', 
				'width' : ' 30%',
				'id' : 'password-field',
				'placeholder' : 'Password'
			}
		)
	)