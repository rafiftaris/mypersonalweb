$.ajax({
	url : "{% url 'book_data' %}",
	dataType : 'json',
	success : function(result){
		var bookList = jQuery.parseJSON(result)
		console.log("success");
		jsontoHTML(bookList);
	}
})

var table = document.getElementById("table");

function jsontoHTML(bookList){
	htmlString = "<tbody>";
	for(var i = 0;i<bookList.items.length;i++){
		authors = "";
		for(j = 0;j < bookList.items[i].volumeInfo.authors.length;j++){
			authors += bookList.items[i].volumeInfo.authors[j];
			if(j != bookList.items[i].volumeInfo.authors.length-1){
				authors += ", ";
			}
		} 
		htmlString += "<tr>" +
		"<td>" + "<img src='" + bookList.items[i].volumeInfo.imageLinks.thumbnail + "'></img>" + "</td>" +
		"<td>" + bookList.items[i].volumeInfo.title + "</td>" +
		"<td>" + authors + "</td>" +
		"<td>" + bookList.items[i].volumeInfo.publisher + "</td>" +
		"<td>" + bookList.items[i].volumeInfo.publishedDate + "</td>" +
		"<td>" + Bintang + "</td>" +
		"</tr>";
	}
	table.insertAdjacentHTML('beforeend',htmlString + "</tbody>")
}