var progress = setInterval(function () {
    var $bar = $("#bar");

    if ($bar.width() >= 750) {
        clearInterval(progress);
    } else {
        $bar.width($bar.width() + 75);
    }
    $bar.text($bar.width() / 7.5 + "%");
    if ($bar.width() / 7.5 == 100){
      $bar.text("Still working ... " + $bar.width() / 7.5 + "%");
    }
}, 800);

$(document).ready(function() {
  $("#bar").width(750);
  $(".loader").fadeOut(3500);
});