from django import forms

class Status_Form(forms.Form):
	error_messages = {
        'required': 'This field is required',
    }
    
	status = forms.CharField(label='Status', required=True, max_length = 300, widget=forms.Textarea(attrs={'type': 'text','placeholder' : "What's on your mind?"}))