from django.test import TestCase, Client, LiveServerTestCase
from .views import *
from django.urls import resolve, Resolver404
import unittest
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles import finders


class LandingPageTest(TestCase):
	def test_landing_page_url_is_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code,200)

	def test_landing_page_using_landing_page_function(self):
		found = resolve('/')
		self.assertEqual(found.func, landing_page)

	def test_landing_page_using_landing_page_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response,'landing_page.html')

	def test_model_can_create_new_status(self):
		# Creating a new activity
		new_status = Status.objects.create(status = 'I am Frustrated')

		# Retrieving all available activity
		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status,1)

	def test_landing_page_success_and_render_the_result(self):
		test = 'Help Me'
		response_post = Client().post('', {'status' : test})
		self.assertEqual(response_post.status_code,302)

		response = Client().get('')
		html_response = response.content.decode('utf8')
		self.assertIn(test,html_response)

class LandingPageFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(LandingPageFunctionalTest, self).setUp()

    def tearDown(self):
    	self.browser.quit()
    	super(LandingPageFunctionalTest,self).tearDown()

    def test_new_status(self):
    	self.browser.get(self.live_server_url)
    	time.sleep(2)
    	self.assertIn("Rafif's Website", self.browser.title)
    	header_text = self.browser.find_element_by_tag_name('h3').text
    	self.assertIn('Write Your Status Here', header_text)

    	status = self.browser.find_element_by_name('status')
    	submit = self.browser.find_element_by_id('submit_btn')


    	status.send_keys('Coba-coba')
    	time.sleep(5)
    	submit.submit()
    	time.sleep(3)

    	newstatus = self.browser.find_element_by_tag_name('td').text
    	self.assertIn('Coba-coba', newstatus)
    	
    	# Test Logo in Logo div
    	division = self.browser.find_element_by_id('center_logo').text
    	image = self.browser.find_element_by_tag_name('img').text
    	self.assertIn(image,division)
    	
    	# Test footer content in footer div
    	division = self.browser.find_element_by_class_name('jumbotron').text
    	socmed = self.browser.find_element_by_id('socmedID').text
    	self.assertIn(socmed,division)
    	
    	# Test Font CSS
    	css = self.browser.find_element_by_tag_name('p').value_of_css_property('font-family')
    	self.assertIn('Reem Kufi',css)

    	# Test logo size
    	css = self.browser.find_element_by_tag_name('img').value_of_css_property("width")
    	self.assertEqual('150px',css)