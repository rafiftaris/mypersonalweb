from django.shortcuts import render,redirect
from .models import Status
from .forms import Status_Form

# Create your views here.
def landing_page(request):
	form = Status_Form(request.POST or None)
	if (request.method == 'POST'):
		if(form.is_valid()):
			status = request.POST.get("status")
			Status.objects.create(status=status)
			return redirect('landing_page:landing_page')

	else:
		status2 = Status.objects.all().order_by('-id')
		response = {
			"status2" : status2
		}
		response['form'] = form
		return render(request,'landing_page.html',response)

def profile(request):
	return render(request,'profile.html',{})