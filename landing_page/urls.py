from django.urls import path
from .views import *

app_name = 'landing_page'
urlpatterns = [
	path('',landing_page, name='landing_page'),
]