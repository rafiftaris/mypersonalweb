var counter = 0;
function favorite(id){
	var button = document.getElementById(id);

	if(button.classList.contains("checked")){
		$.ajax({
        	url: "remove/",
    		success: function(result) {
    			$('#notif').hide();
				var counter = JSON.parse(result);
				console.log(result)
				button.classList.remove("checked");
				document.getElementById("fav-counter").innerHTML = counter;
				button.style.backgroundColor = "#1E90FF"
			},
			error:function(){
				$('#notif').show();
			}
   		 });
	}
	else{
		$.ajax({
    		url: "add/",    		
    		success: function(result) {
    			$('#notif').hide();
				var counter = JSON.parse(result);
				console.log(result)
				button.classList.add("checked");
				document.getElementById("fav-counter").innerHTML = counter;
				button.style.backgroundColor = "#FF5858"

			},
			error:function(){
				$('#notif').show();
			}
		});
	}
}

function searchBook(){
  var keyword = $("#input").val();
  $.ajax({
    url: "data/" + keyword + "/",
    success : function(result){
      var bookList = result.items;
      console.log(bookList);
      jsontoHTML(bookList);
    },
    error: function(){
      console.log('error')
    }
  })
}

$(document).ready(function(){
	$.ajax({
    url: "data/quilting/",
    success : function(result){
      var bookList = result.items;
      console.log(bookList);
      jsontoHTML(bookList);
    },
    error: function(){
      console.log('error')
    }
  })
})

function jsontoHTML(bookList){
	var table = document.getElementById("table");
	htmlString = "<tbody>";
	for(var i = 0;i<bookList.length;i++){
		authors = "";
		for(j = 0;j < bookList[i].volumeInfo.authors.length;j++){
			authors += bookList[i].volumeInfo.authors[j];
			if(j != bookList[i].volumeInfo.authors.length-1){
				authors += ", ";
			}
		} 
		htmlString += "<tr>" +
		"<td>" + "<img src='" + bookList[i].volumeInfo.imageLinks.thumbnail + "'></img>" + "</td>" +
		'<td id="title-' + i + '">' + bookList[i].volumeInfo.title + "</td>" +
		"<td>" + authors + "</td>" +
		"<td>" + bookList[i].volumeInfo.publisher + "</td>" +
		"<td>" + bookList[i].volumeInfo.publishedDate + "</td>" +
		"<td>" + '<button class="btn" onclick=favorite(this.id) id="'+ i +'"><i class="fa fa-star"></i></button>' + "</td>" +
		"</tr>";
	}
	table.insertAdjacentHTML('beforeend',htmlString + "</tbody>")
	$('#table').DataTable();
	$('.dataTables_length').addClass('bs-select');
}
	