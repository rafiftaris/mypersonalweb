var progress = setInterval(function () {
    var $bar = $("#bar");

    if ($bar.width() >= 750) {
        clearInterval(progress);
    } else {
        $bar.width($bar.width() + 75);
    }
    $bar.text($bar.width() / 7.5 + "%");
    if ($bar.width() / 7.5 == 100){
      $bar.text("Still working ... " + $bar.width() / 7.5 + "%");
    }
}, 800);

$(document).ready(function() {
  $("#bar").width(750);
  $(".loader").fadeOut(3500);
});

// var counter = 0;
// function favorite(id){

//   var button = document.getElementById(id);
//   document.getElementById("fav-counter") = counter;

//   if(button.style.backgroundColor == "#1E90FF"){
//     counter++;
//     document.getElementById("fav-counter") = counter;
//     button.style.backgroundColor = "#FF5858"
//   }
//   else{
//     counter--;
//     document.getElementById("fav-counter") = counter;
//     button.style.backgroundColor = "#1E90FF"
//   }
// }