from django.conf.urls import url, include
from django.urls import path
from .views import *
from django.contrib.auth import views

app_name = 'my_library'
urlpatterns = [
	path('', my_library,name='my_library'),
	path('data/<keyword>/', book_data, name='book_data'),
	path('add/', addFav, name='addFav'),
	path('remove/', removeFav, name='removeFav'),
	path('login/', views.LoginView, name='login'),
 	path('logout/', log_out, name='logout'),
]