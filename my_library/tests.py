from django.test import TestCase,Client
from django.urls import resolve
from .views import *


# Create your tests here.
class MysiteTest(TestCase):
	def test_my_library_url_is_exist(self):
		response = Client().get('/my_library/')
		self.assertEqual(response.status_code,200)

	def test_my_library_using_my_library_function(self):
		found = resolve('/my_library/')
		self.assertEqual(found.func, my_library)

	def test_my_library_using_landing_page_template(self):
		response = Client().get('/my_library/')
		self.assertTemplateUsed(response,'my_library.html')