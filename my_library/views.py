from django.http import JsonResponse,HttpResponse
from django.shortcuts import render
from .models import *
import requests
import urllib.request, json
from django.contrib.auth import logout
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth import authenticate, login, logout

# Create your views here.

def index(request):
	return render(request,'index.html')

def book_data(request,keyword):
	URL = "https://www.googleapis.com/books/v1/volumes?q=" + keyword
	getJson = requests.get(URL)
	data = getJson.json()
	if request.user.is_authenticated:
		counter = request.session.get('counter', 0)
		request.session['counter'] = counter
		for key, value in request.session.items():
			print('{} => {}'.format(key, value))
	return JsonResponse(data)

def log_out(request):
	logout(request)
	return HttpResponseRedirect('/my_library/')

def addFav(request):
	print(dict(request.session))
	request.session['counter'] = request.session['counter'] + 1
	return HttpResponse(request.session['counter'], content_type = 'application/json')

def removeFav(request):
	request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type = 'application/json')

def my_library(request):
	response={'counter' : 0}
	if request.user.is_authenticated:
		request.session['user'] = request.user.username
		request.session['email'] = request.user.email
		counter = request.session.get('counter', 0)
		print(dict(request.session))
		response = {'counter' : counter}
		for key, value in request.session.items():
			print('{} => {}'.format(key, value))
	return render(request,'my_library.html',response)